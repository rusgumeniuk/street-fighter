import { createFighterContainer } from "./fighterView";
import { showFighterDetailsModal } from "./modals/fighterDetails";
import { createElement } from "./helpers/domHelper";
import { fight } from "./fight";
import { showWinnerModal } from "./modals/winner";
import { FighterDetails } from "./fighterDetails";
import { DomElement } from "./helpers/domElement";
import { Fighter } from "./fighter";
import { getFighterDetails } from "./services/fightersService";

const fightersDetailsCache = new Map<string, FighterDetails>();

export function createFighters(fighters: Fighter[]) {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map((fighter) =>
    createFighterContainer(fighter, showFighterDetails, selectFighterForBattle)
  );
  const fightersContainer = createElement(new DomElement("div", "fighters"));
  fightersContainer.append(...fighterElements);
  return fightersContainer;
}

async function showFighterDetails(event: Event, fighter: Fighter) {
  const fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

function createFightersSelector() {
  const selectedFighters = new Map<string, FighterDetails>();

  return async function selectFighterForBattle(
    event: Event,
    fighter: FighterDetails
  ) {
    const fullInfo = await getFighterInfo(fighter._id);
    const checkBox = event.target as HTMLInputElement;
    if (!checkBox) {
      return;
    }

    if (checkBox.checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else {
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      const fighters = Array.from(selectedFighters.values());
      const winner = fight({ ...fighters[0] }, { ...fighters[1] });
      showWinnerModal(winner as Fighter);
    }
  };
}

export async function getFighterInfo(
  fighterId: string
): Promise<FighterDetails> {
  if (!fightersDetailsCache.has(fighterId)) {
    const fighter: FighterDetails = await getFighterDetails(fighterId);
    fightersDetailsCache.set(fighter._id, fighter);
  }

  return fightersDetailsCache.get(fighterId);
}
