import { callApi } from "../helpers/apiHelper";
import { Fighter } from "../fighter";
import { FighterDetails } from "../fighterDetails";

export async function getFighters(): Promise<Fighter[]> {
  try {
    const endpoint: string = "fighters.json";
    return (await callApi(endpoint, "GET")) as Promise<Fighter[]>;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id: string): Promise<FighterDetails> {
  try {
    const endpoint: string = `details/fighter/${id}.json`;
    return (await callApi(endpoint, "GET")) as Promise<FighterDetails>;
  } catch (error) {
    throw error;
  }
}
