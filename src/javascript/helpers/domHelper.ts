import { DomElement } from "./domElement";

export function createName(name: string) {
  return createSpanElement(name, "name");
}

export function createSpanElement(value: string, className?: string) {
  const spanElement = createElement(new DomElement("span", className));
  spanElement.innerText = value;
  return spanElement;
}

export function createImage(source: string) {
  const attributes = { src: source };
  const imgElement = createElement(
    new DomElement("img", "fighter-image", attributes)
  );
  return imgElement;
}

export function createCheckbox() {
  const label = createElement(new DomElement("label", "custom-checkbox"));
  const span = createElement(new DomElement("span", "checkmark"));
  const attributes = { type: "checkbox" };

  const checkboxElement = createElement(
    new DomElement("input", "", attributes)
  );

  label.append(checkboxElement, span);
  return label;
}

export function createElement(newElement: DomElement) {
  const element = document.createElement(newElement.tagName);
  if (newElement.className) {
    element.classList.add(newElement.className);
  }
  Object.keys(newElement.attributes).forEach((key) =>
    element.setAttribute(key, newElement.attributes[key])
  );

  return element;
}
