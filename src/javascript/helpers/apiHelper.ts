import { fightersDetails, fighters } from "./mockData";
import { API_URL } from "../constants/apiURL";
import { FighterDetails } from "../fighterDetails";

const useMockAPI = true;

export async function callApi(endpoint: string, method: string) {
  const url = API_URL + endpoint;
  const options = {
    method,
  };

  return useMockAPI
    ? fakeCallApi(endpoint)
    : fetch(url, options)
        .then((response) =>
          response.ok
            ? response.json()
            : Promise.reject(Error("Failed to load"))
        )
        .then((result) => JSON.parse(atob(result.content)))
        .catch((error) => {
          throw error;
        });
}

async function fakeCallApi(endpoint: string) {
  const response =
    endpoint === "fighters.json" ? fighters : getFighterById(endpoint);

  return new Promise((resolve, reject) => {
    setTimeout(
      () => (response ? resolve(response) : reject(Error("Failed to load"))),
      500
    );
  });
}

 function getFighterById(endpoint: string): FighterDetails {
  const start = endpoint.lastIndexOf("/");
  const end = endpoint.lastIndexOf(".json");
  const id = endpoint.substring(start + 1, end);

  return fightersDetails.find((fighter) => fighter._id === id);
}