export class DomElement {
  tagName: string;
  className: string;
  attributes: { [key: string]: string };

  constructor(
    tagName: string,
    className?: string,
    attributes?: { [key: string]: string }
  ) {
    this.tagName = tagName;
    this.className = className;
    this.attributes = attributes ?? {};
  }
}
