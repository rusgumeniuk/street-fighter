import { createElement, createSpanElement } from "../helpers/domHelper";
import { DomElement } from "../helpers/domElement";
import { ModalWindow } from "./modalWindow";

export function showModal(modal: ModalWindow) {
  const root = getRootElement();
  const modalLayer = createModalLayer(modal);

  root.append(modalLayer);
}

function getRootElement() {
  return document.getElementById("root");
}

function createModalLayer(modal: ModalWindow) {
  const modalLayer = createElement(new DomElement("div", "modal-layer"));
  const modalContainer = createElement(new DomElement("div", "modal-root"));
  const modalContainerHeader = createModalContainerHeader(modal.title);    

  modalContainer.append(modalContainerHeader, modal.bodyElement);
  modalLayer.append(modalContainer);

  return modalLayer;
}

function createModalContainerHeader(title: string) {
  const headerElement = createElement(new DomElement("div", "modal-header"));
  const titleElement = createSpanElement(title);
  const closeButton = createElement(new DomElement("div", "close-btn"));
  
  closeButton.innerText = "x";
  closeButton.addEventListener("click", hideModal);

  headerElement.append(titleElement, closeButton);
  return headerElement;
}

function hideModal() {  
  const modal = document.getElementsByClassName("modal-layer")[0];
  modal?.remove();
}
