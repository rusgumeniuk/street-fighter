import { Fighter } from "../fighter";
import { DomElement } from "../helpers/domElement";
import { createElement, createName, createImage } from "../helpers/domHelper";
import { showModal } from "./modal";
import { ModalWindow } from "./modalWindow";

export function showWinnerModal(fighter: Fighter) {
  const title = "Winner";
  const winnerContainer = createWinnerContainer(fighter);
  showModal(new ModalWindow(title, winnerContainer));
}

function createWinnerContainer(fighter: Fighter): HTMLBodyElement {
  const nameElement = createName(fighter.name);
  const imageElement = createImage(fighter.source);
  const fighterContainer = createElement(new DomElement("div", "fighter"));
  fighterContainer.append(imageElement, nameElement);
  return fighterContainer as HTMLBodyElement;
}
