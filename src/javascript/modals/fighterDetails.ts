import {
  createElement,
  createSpanElement,
  createImage,
} from "../helpers/domHelper";
import { showModal } from "./modal";
import { DomElement } from "../helpers/domElement";
import { FighterDetails } from "../fighterDetails";
import { ModalWindow } from "./modalWindow";

export function showFighterDetailsModal(fighter: FighterDetails) {
  const title = "Fighter info";
  const bodyElement = createFighterDetails(fighter) as HTMLBodyElement;
  showModal(new ModalWindow(title, bodyElement));
}

function createFighterDetails(fighter: FighterDetails) {
  const fighterDetails = createElement(new DomElement("div", "modal-body"));
  const modalData = createElement(new DomElement("div", "modal-data"));
  const nameElement = createModalDataRow(fighter.name, "Name");
  const healthElement = createModalDataRow(
    fighter.health.toString(),
    "Health",
    "Total capability to withstand damage"
  );
  const attackElement = createModalDataRow(
    fighter.attack.toString(),
    "Damage",
    "Base damage applied to the target"
  );
  const defenseElement = createModalDataRow(
    fighter.defense.toString(),
    "Armor",
    "Reduces incoming damage by this amount"
  );

  modalData.append(nameElement, healthElement, attackElement, defenseElement);

  const imageElement = createImage(fighter.source);

  fighterDetails.append(modalData, imageElement);

  return fighterDetails;
}

function createModalDataRow(value: string, title: string, toolTip?: string) {
  const modalDataRow = createElement(new DomElement("div"));
  let titleElement: HTMLElement = null;
  if (toolTip) {
    titleElement = createSpanElement(title + " :", "model-data-field");
    titleElement.appendChild(createSpanElement(toolTip, "tool-tip"));
  } else {
    titleElement = createSpanElement(title + " :");
  }
  const valueElement = createSpanElement(" " + value);

  modalDataRow.append(titleElement, valueElement);
  return modalDataRow;
}
