export class ModalWindow {
    title: string;
    bodyElement: HTMLBodyElement;
  
    constructor(title: string, bodyElement: HTMLBodyElement) {
      this.title = title;
      this.bodyElement = bodyElement;
    }
  }