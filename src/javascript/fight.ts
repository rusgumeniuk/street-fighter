import { FighterDetails } from "./fighterDetails";

export function fight(
  firstFighter: FighterDetails,
  secondFighter: FighterDetails
) {
  let attacker: FighterDetails = firstFighter;
  let defender: FighterDetails = secondFighter;
  do {
    const damage = getDamage(attacker, defender);
    defender.health -= damage > 0 ? damage : 0;
    if (defender.health <= 0) {
      return attacker;
    }

    const temp = defender;
    defender = attacker;
    attacker = temp;
  } while (true);
}

export function getDamage(attacker: FighterDetails, enemy: FighterDetails) {
  return getHitPower(attacker) - getBlockPower(enemy);
}

export function getHitPower(fighter: FighterDetails) {
  const criticalHitChance = getRandomValueFrom1To2Inclusive();
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter: FighterDetails) {
  const dodgeChange = getRandomValueFrom1To2Inclusive();
  return fighter.defense * dodgeChange;
}

function getRandomValueFrom1To2Inclusive() {
  const randomFloat = Math.random();
  return randomFloat + randomFloat * Number.MIN_VALUE + 1;
}
