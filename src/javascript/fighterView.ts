import { createElement, createName, createImage, createCheckbox } from "./helpers/domHelper";
import { DomElement } from "./helpers/domElement";
import { Fighter } from "./fighter";

export function createFighterContainer(
  fighter: Fighter,
  handleClick: Function,
  selectFighter: Function
) {  
  const nameElement = createName(fighter.name);  
  const imageElement = createImage(fighter.source);  
  const checkboxElement = createCheckbox();  
  const fighterContainer = createElement(new DomElement("div", "fighter"));  
  fighterContainer.append(imageElement, nameElement, checkboxElement);

  const preventCheckboxClick = (ev: Event) => ev.stopPropagation();
  const onCheckboxClick = (ev: Event) => selectFighter(ev, fighter);
  const onFighterClick = (ev: Event) => handleClick(ev, fighter);

  fighterContainer.addEventListener("click", onFighterClick, false);
  checkboxElement.addEventListener("change", onCheckboxClick, false);
  checkboxElement.addEventListener("click", preventCheckboxClick, false);

  return fighterContainer;
}
