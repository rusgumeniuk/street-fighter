const path = require('path');

module.exports = {
    entry: './index.ts',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '/dist/'
    },
    module: {
        rules: [{
                test: /\.ts$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: "ts-loader"
                        //,
                        // options: {
                        //     configFile: "./babel.config.js",
                        //     cacheDirectory: true
                        // }
                }
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            }
        ]
    },
    resolve: {
        extensions: ['.ts', '.js', '.css']
    },
    mode: 'development',
    devServer: {
        inline: true
    },
    devtool: "source-map"
}